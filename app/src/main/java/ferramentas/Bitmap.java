package ferramentas;
import android.graphics.Matrix;

public class Bitmap {
    public static android.graphics.Bitmap tranformar(android.graphics.Bitmap bitmap, float escala){
        Matrix matrix = new Matrix();
        matrix.postScale(escala,escala);
        return android.graphics.Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
    public static android.graphics.Bitmap tranformar(android.graphics.Bitmap bitmap, float escala,float rotacao){
        Matrix matrix = new Matrix();
        matrix.postScale(escala,escala);
        matrix.postRotate(rotacao);
        return android.graphics.Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
}

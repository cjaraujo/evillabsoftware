package com.evillab.evillab2dsoftware;

import android.app.Activity;
import android.os.Bundle;

import nucleo.MotorPrincipal;

public class MainActivity extends Activity {

    MotorPrincipal motorGrafico;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        motorGrafico = new MotorPrincipal(this,getWindowManager());
        setContentView(motorGrafico.getMotorGrafico());
    }
    protected void onResume() {
        super.onResume();

        motorGrafico.resumir();
    }

    // This method executes when the player quits the game
    @Override
    protected void onPause() {
        super.onPause();
        motorGrafico.pausar();
    }
}

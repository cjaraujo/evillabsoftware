package cenas;

import com.evillab.evillab2dsoftware.R;

import nodes.BitmapComplexo;
import nodes.Raiz;

public class Teste2 extends BitmapComplexo {
    private boolean subindo = true;
    public float velocidade = 400;
    public Teste2(Raiz raiz, Raiz pai, int srcBitmap) {
        super(raiz, pai, R.drawable.fumaca_missil);
        this.posicao.y = 50;
        this.posicao.x = 50;
    }
    public void preparar(){

    }

    public void processar(double delta) {
        if(this.posicao.y>50){
            subindo = false;
        }
        if(this.posicao.y< -50){
            subindo = true;
        }
        if(subindo){
            this.posicao.y += velocidade*delta;
        }else{
            this.posicao.y -= velocidade*delta;
        }
        if(this.posicaoGlobal.y > 500){
            this.liberarDaMemoria();
        }

    }
}

package cenas;

import android.view.MotionEvent;

import com.evillab.evillab2dsoftware.R;

import entidades.MapaDeInput;
import nodes.BitmapComplexo;
import nodes.Camera;
import nodes.Raiz;

public class Teste extends BitmapComplexo {
    private boolean pulo = false;
    private float velocidadeY = .5f;
    public Teste(Raiz raiz, Raiz pai, int srcBitmap) {
        super(raiz, pai, R.drawable.fundo_fogo);
        this.posicao.y = 50;
        this.posicao.x = 450;
        this.escala = 1;

    }
    public void preparar(){

        Teste2 teste2 = new Teste2(this.raiz,this,0);
        this.adicionarFilho(teste2);
        Camera camera = new Camera(this.raiz,this);
        adicionarFilho(camera);
        this.raiz.getMotorPrincipal().getMotorGrafico().setCameraAtual(camera);
    }
    @Override
    public void processar(double delta) {
        if(pulo) {
            this.escala += velocidadeY*delta;
            this.rotacao += 40*delta;
            this.posicao.y += 100*delta;
        }

    }

    public void input(MapaDeInput mapaDeInput){
        switch (mapaDeInput.getTipoInput()) {

            // Player has touched the screen
            case MapaDeInput.ACAO_MOUSE_CLIQUE:


                pulo = true;
                break;

            // Player has removed finger from screen
            case MapaDeInput.ACAO_MOUSE_SOLTAR:


                pulo = false;
                break;
        }
    }
}


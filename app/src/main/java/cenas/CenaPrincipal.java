package cenas;

import nodes.Node;
import nodes.Raiz;

public class CenaPrincipal extends Node {
    public CenaPrincipal(Raiz raiz, Raiz pai) {
        super(raiz, pai);

    }
    public void preparar(){
        Teste teste = new Teste(this.raiz,this,0);
        this.adicionarFilho(teste);
    }
}

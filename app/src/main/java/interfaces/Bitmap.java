package interfaces;

public interface Bitmap {
    public android.graphics.Bitmap getBitmap();
}

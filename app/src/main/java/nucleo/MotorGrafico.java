package nucleo;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import entidades.MapaDoMouse;
import entidades.Vetor2d;
import nodes.BitmapComplexo;
import nodes.Camera;
import nodes.EntidadeSimples;

public class MotorGrafico extends SurfaceView{


    private SurfaceHolder ourHolder;
    private Canvas canvas;
    private Paint paint;
    private MotorPrincipal motorPrincipal;
    private WindowManager gerenciadorJanela;
    private Camera cameraAtual;
    public MotorGrafico(MotorPrincipal motorPrincipal, WindowManager gerenciadorJanela) {
        super(motorPrincipal.getContexto());
        this.motorPrincipal = motorPrincipal;
        this.gerenciadorJanela = gerenciadorJanela;
        this.cameraAtual = new Camera(motorPrincipal.getRaiz(),motorPrincipal.getRaiz());
        ourHolder = getHolder();
        paint = new Paint();
    }


    public void rendenizar() {
        this.cameraAtual.setPosicaoRelativa(Vetor2d.somar(this.cameraAtual.getPosicaoGlobal(),new Vetor2d(-gerenciadorJanela.getDefaultDisplay().getWidth()/2,-gerenciadorJanela.getDefaultDisplay().getHeight()/2)));
        if (ourHolder.getSurface().isValid()) {
            canvas = ourHolder.lockCanvas();
            canvas.drawColor(Color.argb(255,  26, 128, 182));
            paint.setColor(Color.argb(255,  249, 129, 0));
            paint.setTextSize(45);

            for(EntidadeSimples entidade:this.motorPrincipal.getRaiz().getEntidades()){
                if(entidade instanceof BitmapComplexo){
                    paint.setFilterBitmap(false);
                    paint.setAntiAlias(false);
                    Bitmap novoBitmap = ferramentas.Bitmap.tranformar(((BitmapComplexo)entidade).getBitmap(),((BitmapComplexo) entidade).getEscalaGlobal(),((BitmapComplexo) entidade).getRotacaoGlobal());
                    canvas.drawBitmap(novoBitmap, entidade.getPosicaoGlobal().x - novoBitmap.getWidth()/2- this.cameraAtual.getPosicaoRelativa().x, entidade.getPosicaoGlobal().y- novoBitmap.getWidth()/2- this.cameraAtual.getPosicaoRelativa().y, paint);
                    continue;
                }
            }
            ourHolder.unlockCanvasAndPost(canvas);
        }

    }

    public Camera getCameraAtual() {
        return cameraAtual;
    }

    public void setCameraAtual(Camera cameraAtual) {
        this.cameraAtual = cameraAtual;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {

        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                MapaDoMouse mapaDoMouse = new MapaDoMouse(MapaDoMouse.ACAO_MOUSE_CLIQUE,new Vetor2d());
                this.motorPrincipal.getGerenciadorDeEntrada().eventoDeMouse(mapaDoMouse);
                break;

            case MotionEvent.ACTION_UP:
                MapaDoMouse mapaDoMouseUp = new MapaDoMouse(MapaDoMouse.ACAO_MOUSE_SOLTAR,new Vetor2d());
                this.motorPrincipal.getGerenciadorDeEntrada().eventoDeMouse(mapaDoMouseUp);
                break;
        }
        return true;
    }

}

package nucleo;

import android.content.Context;
import android.util.Log;
import android.view.WindowManager;

import cenas.CenaPrincipal;
import cenas.Teste;
import cenas.Teste2;
import nodes.BitmapSimples;
import nodes.Node;
import nodes.Raiz;

public class MotorPrincipal implements Runnable {

    private final static long FPS_MAXIMO = 60;
    private final static long FPS_MINIMO = 30;

    private MotorGrafico motorGrafico;
    private MotorFisico motorFisico;
    private GerenciadorDeEntrada gerenciadorDeEntrada;
    private Thread gameThread = null;
    private volatile boolean playing;
    private double fps =1;
    private Raiz raiz;

    private double delta = 0;
    private Context contexto;
    public MotorPrincipal(Context contexto, WindowManager gerenciadorJanela) {
        this.contexto = contexto;
        this.motorGrafico = new MotorGrafico(this,gerenciadorJanela);
        this.motorFisico = new MotorFisico(this);
        this.gerenciadorDeEntrada = new GerenciadorDeEntrada(this);
        raiz = new Raiz(this);
    }
    public Context getContexto(){
        return this.contexto;
    }
    public Raiz getRaiz(){
        return this.raiz;
    }


    @Override
    public void run() {
        CenaPrincipal cenaPrincipal = new CenaPrincipal(this.raiz, this.raiz);
        this.raiz.adicionarFilho(cenaPrincipal);
        while (playing) {

            double tempoInicial = System.currentTimeMillis();
            motorFisico.atualizar((delta/1000));
            motorGrafico.rendenizar();
            delta = (System.currentTimeMillis() - tempoInicial);
            if(delta < (1000/FPS_MAXIMO)){
                try {
                    gameThread.sleep((long) (1000/FPS_MAXIMO-delta));
                    delta = 1000/FPS_MAXIMO;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public GerenciadorDeEntrada getGerenciadorDeEntrada(){
        return this.gerenciadorDeEntrada;
    }
    public void pausar() {
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            Log.e("Error:", "joining thread");
        }

    }

    public void resumir() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }
    public MotorGrafico getMotorGrafico(){
        return this.motorGrafico;
    }
    public MotorFisico getMotorFisico(){
        return this.motorFisico;
    }
}

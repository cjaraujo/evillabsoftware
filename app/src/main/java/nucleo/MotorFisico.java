package nucleo;

import android.view.MotionEvent;

import entidades.Vetor2d;
import nodes.EntidadeComplexa;
import nodes.EntidadeEscalada;
import nodes.EntidadeSimples;
import nodes.Node;

public class MotorFisico {
    private MotorPrincipal motorPrincipal;
    public MotorFisico(MotorPrincipal motorPrincipal){
        this.motorPrincipal = motorPrincipal;
    }

    public void atualizar(double delta){
        this.motorPrincipal.getRaiz().processar(delta);
        this.atualizarPosicaoGlobal();
    }
    private void atualizarPosicaoGlobal(){

        for(EntidadeSimples entidade:this.motorPrincipal.getRaiz().getEntidades()){
            this.posicaoSimples(entidade);
            if(entidade instanceof EntidadeEscalada){
                this.escala((EntidadeEscalada) entidade);
            }
            if(entidade instanceof EntidadeComplexa){
                this.rodar((EntidadeComplexa) entidade);
            }
        }
    }
    private void posicaoSimples(EntidadeSimples entidade){
        if(!(entidade.getPai() instanceof EntidadeSimples)){

            entidade.getPosicaoGlobal().atualizar(entidade.getPosicao());
            System.out.println("Posicao"+entidade.getPosicao().y+" | filho:"+entidade.getPosicaoGlobal().y);

            return;
        }
        Vetor2d posicaoFinal = Vetor2d.somar(((EntidadeSimples) entidade.getPai()).getPosicaoGlobal(),entidade.getPosicao());

        entidade.getPosicaoGlobal().atualizar(posicaoFinal);

        //System.out.println("Posicao"+entidade.getPosicao().y+" | Pai:"+((EntidadeSimples) entidade.getPai()).getPosicaoGlobal().y+" | filho:"+entidade.getPosicaoGlobal().y+" | final"+posicaoFinal.y);
    }
    private void escala(EntidadeEscalada entidade){
        if(!(entidade.getPai() instanceof EntidadeEscalada)){
            entidade.setEscalaGlobal(entidade.getEscala());
            Vetor2d posicaoFinal = new Vetor2d();
            return;
        }
        entidade.setEscalaGlobal(entidade.getEscala()*((EntidadeEscalada) entidade.getPai()).getEscalaGlobal());
        Vetor2d posicaoFinal = new Vetor2d();
        posicaoFinal.y =    entidade.getPosicao().y * ((EntidadeEscalada) entidade.getPai()).getEscalaGlobal();
        posicaoFinal.x =    entidade.getPosicao().x  * ((EntidadeEscalada) entidade.getPai()).getEscalaGlobal();
        posicaoFinal.y += ((EntidadeEscalada) entidade.getPai()).getPosicaoGlobal().y;
        posicaoFinal.x += ((EntidadeEscalada) entidade.getPai()).getPosicaoGlobal().x;

        entidade.getPosicaoGlobal().atualizar(posicaoFinal);
    }
    private void rodar(EntidadeComplexa entidade){
        if(!(entidade.getPai() instanceof EntidadeComplexa)){
            entidade.setRotacaoGlobal(entidade.getRotacao());
            return;
        }
        entidade.setRotacaoGlobal(entidade.getRotacao()+((EntidadeComplexa) entidade.getPai()).getRotacaoGlobal());
        Vetor2d posicaoFinal = new Vetor2d();
        Vetor2d posicaoRotacionada = new Vetor2d();
        posicaoRotacionada.x = (entidade.getPosicao().x)*((EntidadeEscalada) entidade.getPai()).getEscalaGlobal();
        posicaoRotacionada.y = (entidade.getPosicao().y)*((EntidadeEscalada) entidade.getPai()).getEscalaGlobal();
        posicaoFinal.x = (float) ((float) (posicaoRotacionada.x * Math.cos(Math.toRadians(((EntidadeComplexa) entidade.getPai()).getRotacaoGlobal()))))
                - (float) (posicaoRotacionada.y * Math.sin(Math.toRadians(((EntidadeComplexa) entidade.getPai()).getRotacaoGlobal())));
        posicaoFinal.y = (float) ((float) (posicaoRotacionada.x * Math.sin(Math.toRadians(((EntidadeComplexa) entidade.getPai()).getRotacaoGlobal()))))
                + (float) (posicaoRotacionada.y * Math.cos(Math.toRadians(((EntidadeComplexa) entidade.getPai()).getRotacaoGlobal())));
        posicaoFinal.y += ((EntidadeEscalada) entidade.getPai()).getPosicaoGlobal().y;
        posicaoFinal.x += ((EntidadeEscalada) entidade.getPai()).getPosicaoGlobal().x;

        entidade.getPosicaoGlobal().atualizar(posicaoFinal);
    }


}

package nucleo;

import entidades.MapaDoMouse;
import nodes.Node;

public class GerenciadorDeEntrada {

    private MotorPrincipal motorGrafico;

    public GerenciadorDeEntrada(MotorPrincipal motorGrafico){

        this.motorGrafico = motorGrafico;
    }

    public void eventoDeMouse(MapaDoMouse mapaDoMouse){
        this.motorGrafico.getRaiz().getFilhos();
        for(Node node:this.motorGrafico.getRaiz().getFilhos()) {
            node.input(mapaDoMouse);
        }
    }


}

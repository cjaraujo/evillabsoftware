package entidades;

public class Vetor2d {
    public float x =0;
    public float y = 0;
    public Vetor2d(){

    }
    public Vetor2d(float x,float y){
        this.x = x;
        this.y = y;
    }
    public void somar(Vetor2d vetor2d){
        this.x = this.x +vetor2d.x;
        this.y = this.y +vetor2d.y;
    }
    public void atualizar(Vetor2d vetor2d){
        this.x =vetor2d.x;
        this.y = vetor2d.y;
    }
    public static Vetor2d somar(Vetor2d vetor1,Vetor2d vetor2){
        Vetor2d novoVetor = new Vetor2d();
        novoVetor.x = vetor1.x +vetor2.x;
        novoVetor.y = vetor1.y +vetor2.y;
        return novoVetor;
    }
}

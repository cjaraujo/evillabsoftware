package entidades;

public class MapaDoMouse extends MapaDeInput{
    private Vetor2d posicao;
    public MapaDoMouse(int tipoInput,Vetor2d posicao) {
        super(tipoInput);
        this.posicao = posicao;
    }
    public Vetor2d getPosicao(){
        return this.posicao;
    }
}

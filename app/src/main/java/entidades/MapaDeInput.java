package entidades;

public class MapaDeInput {

    public final static int ACAO_MOUSE_CLIQUE = 0;
    public final static int ACAO_MOUSE_SOLTAR = 1;

    private int tipoInput;

    public MapaDeInput(int tipoInput){
        this.tipoInput = tipoInput;
    }

    public int getTipoInput(){
        return this.tipoInput;
    }
}

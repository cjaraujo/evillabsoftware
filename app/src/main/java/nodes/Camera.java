package nodes;

import entidades.Vetor2d;

public class Camera extends EntidadeSimples{

    protected Vetor2d posicaoRelativa = new Vetor2d();
    public Camera(Raiz raiz, Raiz pai) {
        super(raiz, pai);
    }
    public void setPosicaoRelativa(Vetor2d posicaoRelativa){
        this.posicaoRelativa = posicaoRelativa;
    }
    public Vetor2d getPosicaoRelativa(){
        return this.posicaoRelativa;
    }
}

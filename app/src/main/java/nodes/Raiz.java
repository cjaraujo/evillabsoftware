package nodes;


import java.util.ArrayList;
import java.util.List;

import nucleo.MotorPrincipal;

public class Raiz {
    protected MotorPrincipal motorPrincipal;
    protected List<Node> filhos = new ArrayList<Node>();
    protected List<Node> filaLiberada = new ArrayList<Node>();
    protected List<EntidadeSimples> entidades = new ArrayList<EntidadeSimples>();
    public Raiz(MotorPrincipal motorPrincipal){
        this.motorPrincipal = motorPrincipal;
    }
    public Raiz(){}

    public MotorPrincipal getMotorPrincipal(){
        return this.motorPrincipal;
    }
    public void processar(double delta){
        for(Node filho:this.filhos){
            filho.processar(delta);
        }
        for(Node liberado:filaLiberada){
            liberado.remover();
        }
    }
    public void adicionarFilho(Node node){
        this.filhos.add(node);
        if(node instanceof EntidadeSimples){
            this.entidades.add((EntidadeSimples)node);
        }
        node.preparar();
    }
    public void filaParaLiberar(Node node){
        filaLiberada.add(node);
    }
    public void removerFilho(Node node){
        this.filhos.remove(node);
        if(node instanceof EntidadeSimples){
            this.entidades.remove((EntidadeSimples)node);
        }
    }
    public List<Node> getFilhos(){

        return this.filhos;
    }
    public List<EntidadeSimples> getEntidades(){
        return this.entidades;
    }

}

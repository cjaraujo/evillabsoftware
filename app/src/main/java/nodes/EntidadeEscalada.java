package nodes;

public class EntidadeEscalada extends EntidadeSimples{
    protected float escala = 1;
    protected float escalaGlobal = 1;
    public EntidadeEscalada(Raiz raiz, Raiz pai) {
        super(raiz, pai);
    }
    public float getEscala(){
        return this.escala;
    }
    public float getEscalaGlobal(){
        return this.escalaGlobal;
    }
    public void setEscalaGlobal(float escalaGlobal){
        this.escalaGlobal = escalaGlobal;
    }
}

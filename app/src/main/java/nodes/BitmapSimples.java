package nodes;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.evillab.evillab2dsoftware.R;

public class BitmapSimples extends EntidadeSimples implements interfaces.Bitmap{
    private Bitmap bitmap;
    public BitmapSimples(Raiz raiz, Raiz pai,int srcBitmap) {
        super(raiz, pai);
        bitmap = BitmapFactory.decodeResource(this.getRaiz().getMotorPrincipal().getMotorGrafico().getResources(),srcBitmap);
    }
    public Bitmap getBitmap(){
        return this.bitmap;
    }
}

package nodes;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class BitmapComplexo extends EntidadeComplexa implements interfaces.Bitmap {
    private Bitmap bitmap;
    public BitmapComplexo(Raiz raiz, Raiz pai,int srcBitmap) {
        super(raiz, pai);
        bitmap = BitmapFactory.decodeResource(this.getRaiz().getMotorPrincipal().getMotorGrafico().getResources(),srcBitmap);
    }
    public Bitmap getBitmap(){
        return this.bitmap;
    }
}

package nodes;

import entidades.MapaDeInput;

public class Node extends Raiz{

    protected Raiz pai;

    protected Raiz raiz;

    protected boolean inputAtivo = false;

    public Node(Raiz raiz, Raiz pai) {
        this.raiz = raiz;
        this.pai = pai;
    }
    public void preparar(){

    }
    public void adicionarFilho(Node node){
        this.filhos.add(node);
        this.pai.adicionarFilho(node);
    }
    public void removerFilho(Node node){
        this.filhos.remove(node);
        this.pai.removerFilho(node);
    }
    final public void remover(){
        this.pai.removerFilho(this);
    }
    final public void liberarDaMemoria(){
        raiz.filaParaLiberar(this);
    }
    public void processar(long delta){

    }
    public Raiz getPai(){
        return this.pai;
    }

    public Raiz getRaiz(){
        return this.raiz;
    }

    protected final void ativarInput(boolean ativo){
        this.inputAtivo = ativo;
    }
    public boolean getInputAtivo(){
        return this.inputAtivo;
    }
    public void input(MapaDeInput mapaDeInput){

    }
}

package nodes;

public class EntidadeComplexa extends EntidadeEscalada {
    protected float rotacao = 0;
    protected float rotacaoGlobal = 0;
    public EntidadeComplexa(Raiz raiz, Raiz pai) {
        super(raiz, pai);
    }
    public float getRotacao(){
        return this.rotacao;
    }
    public float getRotacaoGlobal(){
        return this.rotacaoGlobal;
    }
    public void setRotacaoGlobal(float rotacaoGlobal){
        this.rotacaoGlobal = rotacaoGlobal;
    }
}

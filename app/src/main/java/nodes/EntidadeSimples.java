package nodes;

import entidades.Vetor2d;

public class EntidadeSimples extends Node{
    //posicao em relacao ao pai
    protected Vetor2d posicao = new Vetor2d();
    //posicao dele na tela (posicao do pai + posicao)
    protected Vetor2d posicaoGlobal  = new Vetor2d();
    public EntidadeSimples(Raiz raiz, Raiz pai) {
        super(raiz, pai);

    }
    public Vetor2d getPosicao(){
        return this.posicao;
    }
    public Vetor2d getPosicaoGlobal(){
        return this.posicaoGlobal;
    }

}
